#![allow(dead_code)]
use std::mem;

fn if_statement() {
    let temp = 31;

    if temp > 30 {
        println!("It's really hot outside.");
    } else if temp < 10 {
        println!("It's really cold!");
    } else {
        println!("Temperature is ok");
    }

    let day = if temp > 20 {"sunny"} else {"cloudy"};
    println!("{}", day);

    println!("It is {}", if temp > 20 {"hot"} else if temp < 10 {"cold"} else {"OK"});

    println!("It is {}", 
        if temp > 20 {
            if temp > 30 {"very hot"} else {"hot"}
        } else if temp < 10 {"cold"} else {"OK"}
    );
}

fn while_and_loop(){
    let mut x = 1;

    while x < 2000 {
        x *= 2;
        if x == 64 { continue;}
        println!("{}", x);
    }

    let mut y = 1;
    //while true
    loop {
        y *= 2;
        println!("y = {}", y);
        if y == 1 << 11 {break;}
    }
}

fn for_loop() {
    for x in 1..11 {
        if x == 3 {continue;}

        if x == 8 {break;}
        println!("x = {}", x);
    }

    for (pos, y) in (30..41).enumerate() {
        println!("{}: {}", pos, y);
    }
}

fn match_statement() {
    //UK
    let country_code = 999;

    let country = match country_code {
        1 => "United States",
        44 => "UK",
        46 => "Sweden",
        2..=999 => "unknown",
        _ => "invalid"
    };

    println!("The country with code {} is {}", country_code, country);

}

fn main() {
    match_statement();
    //for_loop();
    //while_and_loop();
    //if_statement();

}
