fn print_value(x: i32) {
    println!("value = {}", x);
}

fn product(x: i32, y: i32) -> i32 {
    x * y
}

fn increase(x: &mut i32) {
    // This means that you are bringing in a reference to x (x: &mut i32)
    // Then below, you deference the x variable to increase the real one by 1.
    *x += 1;
}

fn functions() {
    let mut z = 1;
    increase(&mut z);
    println!("z = {}", z);

    print_value(44);
    let x = 5;
    let y = 3;
    let p = product(x, y);

    println!("{} * {} = {}", x, y, p);
}


// Suppose you want a function that is attached to a particular struct

struct Point {
    x: f64,
    y: f64
}

struct Line {
    start: Point,
    end: Point
}

impl Line {
    fn len(&self) -> f64 {
        let dx = self.start.x - self.end.x;
        let dy = self.start.y - self.end.y;
        (dx * dx + dy * dy).sqrt()
    }
}

fn methods() {
    let p = Point { x:3.0, y: 4.0 };
    let p2 = Point { x:5.0, y:10.0 };
    let myline = Line { start: p, end: p2 };

    println!("length = {}", myline.len());
}

fn say_hello() { println!("Hello Rust!"); }

fn closures() {
    let sh  = say_hello;
    sh();

    let plus_one = |x:i32| -> i32 { x + 1 };
    let a = 6;
    println!("{} + 1 = {}", a, plus_one(a));

    // let plus_two = |x:i32| -> i32 { x + 2 };
    // println!("{} + 2 = {}", a, plus_two(a));

    let mut two = 2;
    // More difficult way of achieving the above:
    {
        let plus_two = |x| {
            let mut z = x;
            z += two;
            z
        };
        println!("{} + 2 = {}", 3, plus_two(3));
    }
    // Cannot simply borrow because the closure uses it first, will have to put a block around the closure, then we can make a reference.
    let borrow_two = &mut two;
    // References:
    // T: by value
    // T&
    // &mut &
    let plus_three = |mut x:i32| x += 3;

    let mut f = 12;

    plus_three(f);
    println!("f = {}", f);

}


fn high_order_functions() {
    fn greater_than(limit: u32) -> impl Fn(u32) -> bool {
        // extend the lifetime
        move |y| y > limit
    }


    fn is_even(x: u32) -> bool {
        x % 2 == 0
    }
    // sum of all even squares < 500
    let limit = 500;
    let mut sum = 0;

    let above_limit = |y| y > limit;

    for i in 0.. {
        let isq = i * i;
        //if isq > limit {
        if above_limit(isq) {
            break;
        } else if is_even(isq) {
            sum += isq;
        }
    }

    println!("{}", sum);
    // infinite sequence
    let sum2 = (0..)
        .map(|x| x*x)
        .take_while(|&x| x < limit)
        .filter(|x:&u32| is_even(*x))
        .fold(0, |sum, x| sum + x);
    println!("hof sum = {}", sum2);

}



fn main() {
    high_order_functions();
    //closures();
    //methods();
    //functions();
}
