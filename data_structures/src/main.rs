use std::mem;

struct Point {
    x: f64,
    y: f64
}

struct Line {
    start: Point,
    end: Point,
}

fn structures() {
    let p = Point { x: 3.0, y: 4.0 };
    println!("point p is at ({} {})", p.x, p.y);

    let p2 = Point { x: 5.0, y: 10.0 };
    let myline = Line { start: p, end: p2 };
}

enum Color {
    Red,
    Green,
    Blue,
    RgbColor(u8, u8, u8), //tuple
    CmykColor { cyan:u8, yellow:u8, magenta:u8, black:u8},
}

fn enums() {
    let c:Color = Color::CmykColor{ cyan: 0, magenta: 0, yellow: 0, black: 255 };

    match c {
        Color::Red => println!("r"),
        Color::Green => println!("g"),
        Color::Blue => println!("b"),
        //Color::RgbColor(0,0,0) => println!("black"),
        Color::CmykColor{ cyan:_, magenta:_, yellow:_, black:255 } => println!("black"),
        Color::RgbColor(r,g,b) => println!("{} {} {}", r, g, b),
        _ => ()
    }
}

fn unions() {
    fn process_value(iof: IntOrFloat) {
        unsafe {
            match iof {
                IntOrFloat { i:42 } => {
                    println!("That's the meaning to life.");
                }
                IntOrFloat { f } => { 
                    println!("value = {}", f);
                }
            };
        }
    }

    union IntOrFloat {
        f:f32,
        i:i32
    }

    let mut iof = IntOrFloat{ i:123 };
    iof.i = 234;

    let value = unsafe { iof.i };
    println!("iof.i = {}", value);

    process_value(IntOrFloat{i:5});
}

fn options() {
    // Option<T>
    let x = 3.0;
    let y = 2.0;

    // Some(z) None
    let result:Option<f64> = 
        if y != 0.0 { Some(x/y) } else { None };
    
    println!("{:#?}", result);

    match result {
        Some(z) => println!("{}/{} = {}", x,y,z),
        None => println!("Cannot divide {} by {}", x,y)
    }
    
    if let Some(z) = result { println!("z = {}", z)}
}

fn arrays() {
    // You don't need to explicitly state i32;5 here
    let mut a:[i32;5] = [1,2,3,4,5];
    
    println!("a has {} elements, first is {}", a.len(), a[0]);
    
    a[0] = 323223;
    println!("a[0] = {}", a[0]);

    // print out array
    println!("{:#?}", a);

    //loop though array
    for i in &a {
        println!("Current number is {:#?}", i);
    }

    if a != [32322, 2,3,4,5] 
    {
        println!("Does not match.")
    }

    // Create an array with 10 z's
    let z = ["z";10];
    println!("{:?}", z);

    //Create an array of 10 1's and loop over it
    let ones = [1u16; 10];
    for i in 0..=ones.len()-1 {
        println!("{}", ones[i]);
    }

    // Show the amount of bytes taken by the ones array.
    println!("The number of bytes of the array is {} bytes", mem::size_of_val(&ones));


    // Creating a 2 x 3 Matrix (2 rows with 3 columns each)
    let mtx : [[f32;3]; 2] = [
        [1.0, 0.0, 0.0],
        [0.0, 2.0, 0.0]
    ];
    println!("{:?}", mtx);

    for i in 0..2 {
        println!("{:?}", &mtx[i]);
    }

    // Print out where the values are the same in a matrix (called a diagonal or in mathy terms a sub jj, where row number == column number)

    for i in 0..mtx.len() {
        for j in 0..mtx[i].len() {
            if i == j {
                println!("mtx[{}][{}] = {}", i, j, mtx[i][j]);
            }
        }
    }
}





fn vectors() {

    let mut a = Vec::new();
    a.push(1);
    a.push(2);
    a.push(3);
    a.push(4);

    println!("a = {:?}", a);

    a.push(22);

    println!("a = {:?}", a);

    println!("a[0] = {}", a[0]);
    println!("a[0] = {}", a[a.len()-1]);

    // What is the type of the numbers when looping through the array?
    
    // usize isize

        // Cannot do:
        // let idx:i32 = 0;
        // println!("a[0] = {}", a[idx]);
        // This is because an signed integer cannot act as an index value (like -1 throws error)
        // Also doesn't make sense that I'm using 32 when I have a 64 bit architecture.

    let idx:usize = 0;
    a[idx] = 312;
    println!("a[0] = {}", a[idx]);

    println!("{:?}", a.get(6));

    match a.get(6) {
        Some(x) => println!("a[6] = {}", x),
        None => println!("Index out of range / No such element")
    };

    // Loop over the vector and print the values
    for x in &a {
        println!("{}", x);
    }

    a.push(44);
    println!("{:?}", a);

    // Create a variable for the last element, popped
    let last_element = a.pop(); // returns an option type (Some | None) (Think of this because what if there were no elements to begin with)
    
    //Note that Some(77) is not an int so we need to print in debug string mode.
    match last_element {
        Some(x) => println!("Last Element = {:?}, a = {:?}", last_element, a),
        None => println!("Array was empty.")
    }

    // Pop everything out of the list until nothing is left.
    while let Some(x) = a.pop() {
        println!("{}", x);
    }

}

fn use_slice(slice: &mut[i32]) {
    println!("{:?}", slice);
    println!("first elem = {}, len = {}", slice[0], slice.len());
    slice[0] = 4321;
    println!("first elem = {}, len = {}", slice[0], slice.len());

}

fn slices() {
    let mut data = [1,2,3,4,5];

    use_slice(&mut data[1..4]);
    //create a slice from entire array and uncommenting would mean the first element 1, will get swapped with 4321
    //use_slice(&mut data);
    println!("{:?}", data);
}

fn strings() {
    // vector of chracters (because they can dynamically grow)
    //let s = "hello there!";
    let s:&'static str = "hello rust!"; // &str = string slice


    // Note that we cannot do this:
    // s = "abc";
    // let h = s[0];
    
    for c in s.chars().rev() {
        println!("{}", c);
    }

    //Safe way to get a character or a string
    if let Some(first_char) = s.chars().nth(0) {
        println!("#######");
        println!("{}", first_char);
    }

    //heap
    //String
    let mut letters = String::new();
    let mut a = 'a' as u8;
    
    while a <= ('z' as u8) {
        letters.push(a as char);
        letters.push_str(",");
        a += 1;
    }

    println!("{}", letters);

    // &str <> String
    // If you want to make a slice (&str) from a String:
    // Deref conversions
    let u:&str = &letters;
    println!("{:?}", u.chars());

    //concatenation
    // String + str
    //let z = letters + "abc";
    let z = letters.to_owned().clone() + &letters;
    println!("{}", z);

    // Make string from str.
    //println!("Coding: {}", abc+" yo");
    let mut abc = String::from("Hello Rust!");
    abc.remove(0);
    abc.push_str("!!!");
    println!("{}", abc.replace("ello", "goodbye"));
}

fn tuples() {

    fn sum_and_product(x:i32, y:i32) -> (i32, i32) {
        (x+y, x*y)
    }

    let x = 0;
    let y = 10;
    let sp = sum_and_product(x,y);

    println!("{:?}", sp);
    println!("{0} + {1} = {2}, {0} * {1} = {3}", x, y, sp.0, sp.1);

    // destructuring | unpacking | tokenizing
    let(a,b) = sp;
    println!("a = {}, b = {}", a,b);

    // Creating Tuple of Tuples
    let sp2 = sum_and_product(4,7);
    let combined = (sp, sp2);
    println!("{:?}", combined);
    println!("last element = {}", (combined.1).1);

    // More unpacking / Destructuring
    let ((c,d),(e,f)) = combined;
    println!("c = {}, d = {}, e = {}, f = {}", c, d, e, f);

    // Print a tuple of different types
    let all_types = (true, 3.1415925635, "Hello Rust!", -1i8);
    println!("{:?}", all_types);

    // Create a tuple of a single element
    let meaning  = (42,);
    println!("Single Element Tuple: {:?}", meaning);
}

fn pattern_matching() {

    fn how_many(x:i32) -> &'static str {
        match x {
            0 => "no",
            1 | 2 => "one or two",
            //9...11 => "lots of",
            // Can also name your ranges if you want to use in a case
            12 => "a dozen",
            z @ 9..=11 => "lots of",
            _ if (x%2 == 0) => { "an even number" },
            _ => "a few",
        }
    }

    for x in 0..13 {
        println!("{}: I have {} oranges", x, how_many(x));
    }

    let point = (2,4);
    match (point) {
        (0,0) => println!("origin"),
        (0,y) => println!("x-axis, y = {}", y),
        (x,0) => println!("x = {}, y-axis", x),
        (x,y) => println!("x = {}, y = {}", x, y),
        (_,y) => println!("(?,{})", y)
    }

    // Matching based on only one value out of entire tuple (from previous example)

    let c:Color = Color::CmykColor{ cyan: 0, magenta: 0, yellow: 0, black: 255 };

    match c {
        Color::Red => println!("r"),
        Color::Green => println!("g"),
        Color::Blue => println!("b"),
        //Color::RgbColor(0,0,0) => println!("black"),
        Color::CmykColor{ black:255,.. } => println!("black"),
        Color::RgbColor(r,g,b) => println!("{} {} {}", r, g, b),
        _ => ()
    }
}


fn generics() {

    // struct Point<T> {
    //     x: f64,
    //     y: f64
    // }

    // Suppose that we want to make points from different data types than the above
    // maybe we want f32 values or to NOT have to specify the type of the coordinate.
    // Add a generic called T, like so: struct Point <T>
    // Note that we've seen <T> types in Option<T>

    struct Point<T> {
        x: T,
        y: T
    }

    struct Line<T> {
        start: Point<T>,
        end: Point<T>
    }

    let a:Point<f64> = Point { x: 0.3, y: 4.0};
    let b:Point<f64> = Point { x: 1.3, y: 3.4};

    let myline = Line { start: a, end: b};

}














fn main() {

    generics();
    //pattern_matching();
    //tuples();
    //strings();
    //slices();
    //vectors();
    //arrays();
    //options();
    //unions();
    //enums();
    //structures()
}
