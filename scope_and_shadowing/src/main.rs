use std::mem;

const MEANING_OF_LIFE:u8 = 42;
static mut z:i32 = 123;

fn scope_and_shadowing() 
{
    let a = 123;
    
    {
        let b = 456;
        let a = 777;
        println!("inside, b {}", b);
        println!("INSIDE a is equal to {}", a);
    }
    
    println!("OUTSIDE a is equal to {}", a);

}

fn main(){
    scope_and_shadowing();
    unsafe {
        z = 777;
        println!("The meaning of life is {}", z);
    }
}